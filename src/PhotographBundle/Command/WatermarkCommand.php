<?php

namespace Mojomaja\Bundle\PhotographBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WatermarkCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('photograph:watermark')
            ->setDescription('Watermark photograph')
            ->addArgument('original', InputArgument::REQUIRED, 'original, a path')
            ->addArgument('backup', InputArgument::OPTIONAL, 'backup, a path')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $watermark = $this->getContainer()->get('mojomaja_photograph.helper.watermark');
        $watermark->mark($input->getArgument('original'), $input->getArgument('backup'));

        $output->writeln('ok.');
    }
}

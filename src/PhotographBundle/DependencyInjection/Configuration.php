<?php

namespace Mojomaja\Bundle\PhotographBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('mojomaja_photograph');

        $rootNode
            ->children()
                ->scalarNode('root')->defaultValue('%kernel.root_dir%/../web/photograph')->end()
                ->integerNode('quality')->defaultValue(85)->end()
                ->arrayNode('watermark')
                    ->children()
                        ->scalarNode('backup')->defaultValue('.o')->end()
                        ->scalarNode('image')->isRequired()->end()
                        ->integerNode('width')->isRequired()->end()
                        ->integerNode('height')->isRequired()->end()
                        ->integerNode('x')->defaultValue(-20)->end()
                        ->integerNode('y')->defaultValue(-15)->end()
                        ->scalarNode('opacity')->defaultValue(.5)->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}

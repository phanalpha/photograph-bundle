<?php

namespace Mojomaja\Bundle\PhotographBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Reference;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class MojomajaPhotographExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $container->setParameter('mojomaja_photograph.root', $config['root']);
        $container->setParameter('mojomaja_photograph.quality', $config['quality']);
        if (!empty($config['watermark']['image'])) {
            foreach (['backup', 'image', 'width', 'height', 'x', 'y', 'opacity'] as $k)
                $container->setParameter(
                    'mojomaja_photograph.watermark.'.$k,
                    $config['watermark'][$k]
                );

            $container
                ->getDefinition('mojomaja_photograph.helper.photograph')
                ->replaceArgument(4, new Reference('mojomaja_photograph.helper.watermark'))
            ;
        }
    }
}

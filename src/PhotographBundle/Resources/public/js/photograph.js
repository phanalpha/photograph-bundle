YUI.add('mojomaja-photograph', function (Y) {
    var create_photograph_url =
	    Y.config.modules &&
	    Y.config.modules[this.name] &&
	    Y.config.modules[this.name]['create_photograph_url'] ||
	    '/photograph/'
    ;

    function Uploader(multiple, watermark) {
	var file_input = Y.Node.create('<input type="file" name="photograph[]">');
	var form = Y.Node.create('<form></form>').append(file_input).hide();
	if (multiple)
	    file_input.setAttribute('multiple', 'multiple');
	if (watermark)
	    form.append(Y.Node.create('<input type="hidden" name="watermark" value="1">'));

	Y.one('body').append(form);

	var that = this;
	file_input.on('change', function () {
	    that.fire('start', {value: file_input.get('value')});

	    Y.io(create_photograph_url, {
		method: 'POST',
		form  : {
		    id    : form,
		    upload: true
		},
		on    : {
		    complete: function (_, resp) {
			form.reset();

			that.fire('complete', Y.JSON.parse(resp.responseText).it);
		    }
		}
	    });
	});

	Y.mix(this, { exec: function () {
	    file_input.simulate('click');
	} });
    }

    Y.mix(
	Y.namespace('Mojomaja.Photograph'),
	{
	    Uploader: Y.augment(Uploader, Y.EventTarget)
	}
    );
}, '0.0.1', {
    requires: ['node', 'node-event-simulate', 'io', 'json-parse', 'event-custom']
});

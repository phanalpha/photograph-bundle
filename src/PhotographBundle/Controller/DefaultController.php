<?php

namespace Mojomaja\Bundle\PhotographBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Mojomaja\Bundle\PhotographBundle\Helper\DemandScenario;

class DefaultController extends Controller
{
    public function createAction(Request $request)
    {
        $watermark  = $request->request->get('watermark');
        $helper     = $this->get('mojomaja_photograph.helper.photograph');
        $scenario   = $this->get('mojomaja_photograph.helper.default_scenario');

        return new JsonResponse([
            'it' => array_map(
                function (UploadedFile $file) use ($helper, $watermark, $scenario) {
                    $localname = $helper->persist($file, $watermark);

                    return [
                        'localname' => $localname,
                        'path'      => $this->generateUrl(
                            'mojomaja_photograph_show', [
                                'path'  => implode('/', $scenario->hash($localname))
                            ]
                        )
                    ];
                },
                $request->files->get('photograph')
            )
        ], JsonResponse::HTTP_OK, [ 'Content-Type' => 'text/plain' ]);
    }

    public function showAction($path)
    {
        list($l1, $l2, $localname) = explode('/', $path);

        $scenario   = $this->get('mojomaja_photograph.helper.default_scenario');
        return new BinaryFileResponse(
            implode(DIRECTORY_SEPARATOR, $scenario->hash(
                $localname,
                $this->container->getParameter('mojomaja_photograph.root')
            ))
        );
    }

    public function demandAction($path)
    {
        list($formula, $size, $l1, $l2, $localname) = explode('/', $path);
        list($width, $height) = explode(',', $size);

        $scenario   = new DemandScenario($formula, (int) $width, (int) $height);
        return new BinaryFileResponse(
            $this
                ->get('mojomaja_photograph.helper.photograph')
                ->compose($localname, $scenario)
        );
    }
}

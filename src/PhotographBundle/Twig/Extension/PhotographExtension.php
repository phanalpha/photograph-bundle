<?php

namespace Mojomaja\Bundle\PhotographBundle\Twig\Extension;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Mojomaja\Bundle\PhotographBundle\Helper\Scenario;
use Mojomaja\Bundle\PhotographBundle\Helper\DemandScenario;

class PhotographExtension extends \Twig_Extension
{
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * @var Scenario
     */
    private $scenario;


    public function __construct(UrlGeneratorInterface $urlGenerator, Scenario $scenario)
    {
        $this->urlGenerator = $urlGenerator;
        $this->scenario     = $scenario;
    }

    public function getName()
    {
        return 'mojomaja_photograph';
    }

    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter(
                'photograph',
                function ($localname, $w = 0, $h = 0, $formula = 'crop') {
                    if ($localname) {
                        if ($w > 0 || $h > 0) {
                            $scenario = new DemandScenario($formula, (int) $w, (int) $h);

                            return $this->urlGenerator->generate(
                                'mojomaja_photograph_demand',
                                [ 'path' => implode('/', $scenario->hash($localname)) ]
                            );
                        }
                        else
                            return $this->urlGenerator->generate(
                                'mojomaja_photograph_show',
                                [ 'path' => implode('/', $this->scenario->hash($localname)) ]
                            );
                    }
                }
            ),

            new \Twig_SimpleFilter(
                'photograph_url',
                function ($localname, $w = 0, $h = 0, $formula = 'crop') {
                    if ($localname) {
                        if ($w > 0 || $h > 0) {
                            $scenario = new DemandScenario($formula, (int) $w, (int) $h);

                            return $this->urlGenerator->generate(
                                'mojomaja_photograph_demand',
                                [ 'path' => implode('/', $scenario->hash($localname)) ],
                                UrlGeneratorInterface::ABSOLUTE_URL
                            );
                        }
                        else
                            return $this->urlGenerator->generate(
                                'mojomaja_photograph_show',
                                [ 'path' => implode('/', $this->scenario->hash($localname)) ],
                                UrlGeneratorInterface::ABSOLUTE_URL
                            );
                    }
                }
            )
        ];
    }
}

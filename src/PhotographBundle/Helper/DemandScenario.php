<?php

namespace Mojomaja\Bundle\PhotographBundle\Helper;

class DemandScenario extends Scenario
{
    const FILL = 'fill';
    const CROP = 'crop';

    private $formula;           /* fill|crop */
    private $width;
    private $height;

    /**
     * @param string    formula
     * @param integer   width
     * @param integer   height
     */
    public function __construct($formula = self::CROP, $width = 0, $height = 0)
    {
        assert(
            ($formula == self::FILL || $formula == self::CROP) &&
            is_integer($width) && $width >= 0 &&
            is_integer($height) && $height >= 0 &&
            ($width > 0 || $height > 0)
        );

        $this->formula  = $width > 0 && $height > 0 ? $formula : self::FILL;
        $this->width    = $width;
        $this->height   = $height;
    }

    public function getFormula()
    {
        return $this->formula;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    protected function compose($localname)
    {
        return [
            $this->getFormula(),
            implode(',', [ $this->getWidth(), $this->getHeight() ]),
            substr($localname, 0, 2),
            substr($localname, 2, 2),
            $localname
        ];
    }
}

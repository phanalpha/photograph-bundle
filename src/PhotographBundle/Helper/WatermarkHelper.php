<?php

namespace Mojomaja\Bundle\PhotographBundle\Helper;

class WatermarkHelper
{
    private $image;

    private $width;

    private $height;

    private $x;

    private $y;

    private $opacity;


    /**
     * @param string    $image      path, the
     * @param integer   $width      canvas width
     * @param integer   $height     canvas height
     * @param integer   $x
     * @param integer   $h
     * @param float     $opacity
     */
    public function __construct($image, $width, $height, $x, $y, $opacity)
    {
        $this->image        = $image;
        $this->width        = $width;
        $this->height       = $height;
        $this->x            = $x;
        $this->y            = $y;
        $this->opacity      = $opacity;
    }

    /**
     * @param string    $original
     * @param string    $backup
     */
    public function mark($original, $backup = null)
    {
        if ($backup) {
            @mkdir(dirname($backup), 0777, true);
            @rename($original, $backup);
        }
        else
            $backup = $original;

        $wm = new \Imagick();
        $wm->setBackgroundColor(new \ImagickPixel('transparent'));
        $wm->readImage($this->image);
        $wm_w = $wm->getImageWidth();
        $wm_h = $wm->getImageHeight();

        $im = new \Imagick($backup);
        $im_w = $im->getImageWidth();
        $im_h = $im->getImageHeight();

        $z = min($im_w / $this->width, $im_h / $this->height);
        $wm->resizeImage($wm_w * $z, $wm_h * $z, \Imagick::FILTER_UNDEFINED, 1);
        $wm->evaluateImage(
            \Imagick::EVALUATE_MULTIPLY,
            $this->opacity,
            \Imagick::CHANNEL_ALPHA
        );
        $im->compositeImage(
            $wm,
            \Imagick::COMPOSITE_DEFAULT,
            (($this->x > 0 ? $this->x : $this->x - $wm_w) * $z + $im_w) % $im_w,
            (($this->y > 0 ? $this->y : $this->y - $wm_h) * $z + $im_h) % $im_h
        );
        $im->writeimage($original);
    }
}

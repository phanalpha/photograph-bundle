<?php

namespace Mojomaja\Bundle\PhotographBundle\Helper;

class Scenario
{
    public function hash($localname, $root = null)
    {
        $s = $this->compose($localname);
        if ($root)
            array_unshift($s, $root);

        return $s;
    }

    protected function compose($localname)
    {
        return [
            substr($localname, 0, 2),
            substr($localname, 2, 2),
            $localname
        ];
    }
}

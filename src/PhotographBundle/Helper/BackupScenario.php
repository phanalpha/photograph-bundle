<?php

namespace Mojomaja\Bundle\PhotographBundle\Helper;

class BackupScenario extends Scenario
{
    private $backup;

    public function __construct($backup)
    {
        $this->backup = $backup;
    }

    protected function compose($localname)
    {
        return [
            $this->backup,
            substr($localname, 0, 2),
            substr($localname, 2, 2),
            $localname
        ];
    }
}

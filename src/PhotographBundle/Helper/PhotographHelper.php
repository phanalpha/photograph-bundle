<?php

namespace Mojomaja\Bundle\PhotographBundle\Helper;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

class PhotographHelper
{
    /**
     * @var string
     */
    private $root;

    /**
     * @var Scenario
     */
    private $primary;

    /**
     * @var Scenario
     */
    private $secondary;

    /**
     * @var Watermark
     */
    private $watermark;

    /**
     * @var int
     */
    private $quality;

    /**
     * @var LoggerInterface
     */
    private $logger;


    public function __construct($root, Scenario $primary, Scenario $secondary = null, $quality = 85, WatermarkHelper $watermark = null, LoggerInterface $logger = null)
    {
        $this->root         = $root;
        $this->primary      = $primary;
        $this->secondary    = $secondary;
        $this->quality      = $quality;
        $this->watermark    = $watermark;
        $this->logger       = $logger;
    }

    public function persist($file, $watermark = false)
    {
        $localname  = sha1_file($file) . $this->infer_extension($file);

        $path   = implode(DIRECTORY_SEPARATOR, $this->primary->hash($localname, $this->root));
        $dir    = dirname($path);
        @mkdir($dir, 0777, true);
        if ($file instanceof File)
            $file->move($dir, $localname);
        else
            rename($file, $path);

        if ($watermark && $this->watermark) {
            $this->watermark->mark(
                $path,
                implode(DIRECTORY_SEPARATOR, $this->secondary->hash($localname, $this->root))
            );
        }

        return $localname;
    }

    private function infer_extension($file)
    {
        $ext = image_type_to_extension(exif_imagetype($file));
        if ($ext)
            return $ext;

        if ($file instanceof UploadedFile) {
            $ext = $file->guessClientExtension();
            if (!$ext)
                $ext = $file->getClientOriginalExtension();
        }
        if ($file instanceof File) {
            $ext = $file->guessExtension();
            if (!$ext)
                $ext = $file->getExtension();
        }

        $ext = (new \SplFileInfo($file))->getExtension();
        if ($ext)
            return '.' . $ext;
    }

    public function compose($localname, DemandScenario $scenario)
    {
        $src        = implode(DIRECTORY_SEPARATOR, $this->secondary->hash($localname, $this->root));
        $watermark  = $this->watermark;
        if (!file_exists($src)) {
            $src        = implode(DIRECTORY_SEPARATOR, $this->primary->hash($localname, $this->root));
            $watermark  = null;
        }
        $dst    = implode(DIRECTORY_SEPARATOR, $scenario->hash($localname, $this->root));
        $w      = $scenario->getWidth();
        $h      = $scenario->getHeight();

        @mkdir(dirname($dst), 0777, true);
        $im     = new \Imagick($src);
        switch ($scenario->getFormula()) {
            case DemandScenario::FILL:
                if ($w > 0 && $h > 0)
                    $im->thumbnailimage($w, $h, true);
                else
                    $im->scaleimage($w, $h);
                break;

            case DemandScenario::CROP:
                $im->cropthumbnailimage($w, $h);
                break;
        }
        if (null !== $this->logger)
            $this->logger->debug(sprintf('Quality: %d / %d.', $this->quality, $im->getImageCompressionQuality()));
        if ($this->quality < $im->getImageCompressionQuality())
            $im->setImageCompressionQuality($this->quality);
        $im->writeimage($dst);

        if ($watermark)
            $watermark->mark($dst);

        return $dst;
    }
}
